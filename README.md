# Escape-from-North-korea

-------------
## 1. 목차
```
.THE.탈북
├── 1.목차
├── 2.개요
├── 3.특징
├── 4.개발 환경 & 개발 툴
└── 5.실행 영상
```



## 2. 개요
 - **Unity**로 구현한 **3D 액션 게임**입니다. 


 
## 3. 특징
- 모든 맵을 직접 제작하였습니다.



## 4. 개발 환경 & 개발 툴
- Programing Language : C#
- OS: Windows 10
-----------------------------------------------------------------------------
- Tool : Unity
- Graphic Tool : Adobe Photoshop CC, 3Ds MAX


## 5. 실행 영상
 [![video](https://i.ytimg.com/vi/9kogK6KdYb8/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAkiw4yecKM-Ll2BpKvClvlGEGS6Q)](https://www.youtube.com/embed/9kogK6KdYb8?start=9)
 -------------
### 위의 이미지를 클릭하면 해당 영상으로 이동합니다.

-----------------------------------------------------------------------------
# 감사합니다!
