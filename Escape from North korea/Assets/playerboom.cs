﻿using UnityEngine;
using System.Collections;

public class playerboom : MonoBehaviour 
{
	public float moveSpeed = 0.2f;
	public int Hp = 2;
	// Use this for initialization
	void Update ()
	{
		
		float h = Input.GetAxis ("Horizontal");
		
		transform.position += new Vector3(h, 0f, 0f) * moveSpeed * Time.deltaTime;
		
		
		if(Hp <= 0 )
		{
			Destroy(gameObject);
		}
	}
	
	void ReduceHp()
	{
		Hp--;
	}
	
}
