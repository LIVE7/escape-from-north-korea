﻿using UnityEngine;
using System.Collections;

public class BGM : MonoBehaviour {

	public AudioClip[] bgm;
	public AudioClip fly;
	public PlayerControl PC;
	public AudioSource AS;
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(PC.fly)
		{
			AS.clip = fly;
			if(!AS.isPlaying)
			{

				AS.Play();
			}
		}
		else
		{
			if(AS.clip == fly)
			{
				AS.clip = bgm[Random.Range(0, bgm.Length)];
			}

			if(!AS.isPlaying)
			{
				AS.clip = bgm[Random.Range(0, bgm.Length)];
				AS.Play();
			}
		}




	}
}
