﻿using UnityEngine;
using System.Collections;

public class player123 : MonoBehaviour {

	public GameObject explode;
	public Transform player;
	public AudioClip bomb;
	AudioSource AS;
	void Start ()
	{
		AS = GetComponent<AudioSource> ();
		//this.GetComponent<Rigidbody>().velocity = new Vector3(0.0f, 0.0f, 0.0f);
	}
	
	// Update is called once per frame

	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.tag == "ground")
		{
			if(player != null)
			{
				if(Vector3.Distance(player.position, transform.position) < 5)
				{

					player.SendMessage("ReduceHp");
				}
			}

			Instantiate(explode , transform.position, explode.transform.rotation);

			Destroy(gameObject);
		}
}
}