﻿using UnityEngine;
using System.Collections;

public class Explode : MonoBehaviour {
	public AudioSource AS;
	public AudioClip bomb;
	void Start ()
	{
		//AS = GetComponent<AudioSource> ();
		AS.clip = bomb;
		AS.Play ();
		Destroy (gameObject, 2);
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
